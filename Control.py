"""
Author: Christopher Ball
Create Date: 25/09/2017
"""
from Database import *
from Utilities import HTMLStatistics
import datetime

DB = Database()

Participants = ['Leonard Reeves','Alan Griffin','Rob Pollock',
                'Roger Tucker','Dan Ellis','Chris Haynes',
                'Matt George','Luke Clark-Grayling','Chris Ball',
                'David Warwick','Mark McGregor','Allan Heald',
                'Peter Fisk','Shone Holland','Grant Ashby','Liz Roberts',
                'Peter Chapman','Julie Burt','Michael Burt','Dayna Haylock',
                'Corey Haylock','Marcus Houghton','Bradley West','Ash Ball',
                'Phil Wright','Terry Mustchin','Fred Housham','Don Adshead',
                'Yoyo Muljono','Ivena Heald','Alysha Carr','Stephanie George','Dyanni Ross',
                'Amy Mustchin', 'Colin McLean', 'Wink Mustchin', 'Stephen Penny',
                'Christopher MacDonald', 'Janet Miklos', 'Donna Fletcher',
                'Cushla Burns', 'Chelsie Parsons', 'Lynnaire Churchill',
                'Carole Adshead', 'Gavin Povey', 'Alexi Lourie', 'Suzanne Howell',
                'Sam Kidd', 'Alvin Kidd', 'Ramon Tui', 'Lance Barns',
                'Kerrie Dee', 'Delfin De Guzman', 'Tina Sicat', 'Andy Smith',
                'Mike Gibbs', 'Hamish McGrigor', 'Clare Sahayam',
                'Kevin Foubister', 'Dairne Kaimoana', 'Phil Turner', 'Jess Guptill',
                'Nicole Wallace', 'John Crawford', "Nicki Crawford", "Jason Peters",
                "Diane Mclean", "Angus Speers", "Benjamin King", "Justin Dick",
                "Shane Devine", "Devan Sahayam", "Chris Sahayam", "Greg Nicolas",
                "Lil Plunkett", "David Brierley", "Robert Tui", "Will Pettit",
                "Ben Pettit", "Tony Payne", "Paul Pettit", "Matt McErlean",
                "Coco Tuason", "Brent Hardy", "Annabelle Swain", "Ian Jenkins",
                "Craig Nevatt", "Tim Swain", "Mark Francis", "Jess Swain",
                "Chris Armstrong", "Danual Paton", "Kelly Paton",
                "Stephen Pitt", "Dion Lee", "Chris McLaren", "Joel Crack",
                "Josh Stretton", "Roger Balanay", "Kaleb Allardyce", "Mitchell Goggins",
                "Rey Non", "Glen Deal", "Ian Klein", "Jho Balanay",
                "Wayne Booth", "Judy Booth", "Rebecca Clark", "Ink Tay",
                "Phillip Mallite", "Kurt Zvagulis", "Claudia Armstrong",
                "Prime Castro", "Stuart Cunningham", "Lachlan Armstrong",
                "Jayde Crawley", "Hannah Scott", "Alex Harman",
                "Kerry Richmond", "Alex Harman", "Michael Reed",
                "David Rickard", "Julie Knight", "Pete Richardson",
                "Coralie Duignan"]

path = 'C:/Users/Admin/Documents/Bowling/Tournaments/Tauranga Classic/Source/'

DB.ImportFile(path + 'PrintPlayerGame_20170924_1511.xls',
              Participants, Series_Name = "Qualifying")

# Correctly identify the series
Squad_1 = datetime.datetime(2017, 9, 23, 12, 30)
Squad_2 = datetime.datetime(2017, 9, 23, 23, 59)
Squad_3 = datetime.datetime(2017, 9, 24, 11, 00)

for P in DB.Players:
    for G in P.Games:
        if G.Meta[2] < Squad_1: G.Series_Desc = "Qualifying - Squad B"
        elif G.Meta[2] < Squad_2: G.Series_Desc = "Qualifying - Squad C"
        elif G.Meta[2] < Squad_3 and G.Meta[0] > 10: G.Series_Desc = "Senior Masters"
        elif G.Meta[2] < Squad_3 and G.Meta[0] <= 10: G.Series_Desc = "Ladies Masters"
        else: G.Series_Desc = "Open Masters"


Series = {}
for P in DB.Players:
    for G in P.Games:
        try: Series[P.Name, G.Series_Desc].append(G.GetSS())
        except: Series[P.Name, G.Series_Desc] = [G.GetSS()]
        print(G.Meta)

for skey in sorted(Series.keys()):
    print(",".join(skey), ",".join(str(e) for e in Series[skey]), ",", sum(Series[skey]))

HTMLStatistics(DB, "2017 Tauranga Classic", "Tenpin Tauranga", "September 23rd to 24th")
